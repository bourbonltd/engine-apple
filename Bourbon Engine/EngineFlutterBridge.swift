import Foundation
import Flutter

class EngineFlutterBridge {
    weak var delegate: BourbonEngineDelegate?
    let bourbonEngineChannel: FlutterMethodChannel

    //swiftlint:disable cyclomatic_complexity line_length
    init(flutterEngine: FlutterEngine) {
        bourbonEngineChannel = FlutterMethodChannel(name: "bourbon.sh/engine",
                                                    binaryMessenger: flutterEngine.binaryMessenger)

        bourbonEngineChannel.setMethodCallHandler({ [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            guard let self = self else { return }
            switch call.method {
            case "bootstrapped":
                self.delegate?.bootstrapped()
            case "routeLoaded":
                if let route = self.extractRouteFromArguments(arguments: call.arguments) {
                    self.delegate?.routeLoaded(route: route)
                }
            case "routeError":
                if let route = self.extractRouteFromArguments(arguments: call.arguments) {
                    self.delegate?.routeError(route: route)
                }
            case "routeChanged":
                if let route = self.extractRouteFromArguments(arguments: call.arguments) {
                    self.delegate?.routeChanged(newRoute: route)
                }
            case "tap":
                if let arguments = call.arguments as? [String: Any],
                    let action = arguments["action"] as? String,
                    let system = arguments["system"] as? Bool {
                        self.delegate?.tap(action: action, system: system)
               }
            case "error":
                self.delegate?.error()
            default:
                result(FlutterMethodNotImplemented)
            }
        })
    }
    //swiftlint:enable cyclomatic_complexity line_length

    private func extractRouteFromArguments(arguments: Any?) -> String? {
        guard let arguments = arguments as? [String: String], let route = arguments["route"] else {
            return nil
        }
        return route
    }

    func setup(engineConfiguration: EngineConfiguration) {
        if let jsonData = try? JSONEncoder().encode(engineConfiguration) {
            let jsonString = String(data: jsonData, encoding: .utf8)
            bourbonEngineChannel.invokeMethod("setup", arguments: jsonString)
        }
    }

    func updateRoute(route: EngineRoute, behaviour: RouteBehaviour) {
        let engineRoute = EngineRouteChange(route: route.route, behaviour: behaviour, properties: route.properties)
        if let jsonData = try? JSONEncoder().encode(engineRoute) {
            let jsonString = String(data: jsonData, encoding: .utf8)
            bourbonEngineChannel.invokeMethod("updateRoute", arguments: jsonString)
        }
    }
}
