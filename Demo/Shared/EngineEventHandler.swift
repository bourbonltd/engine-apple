import Foundation
import BourbonEngine

class EngineEventHandler: BourbonEngineDelegate {
    func bootstrapped() {
        debugPrint("Engine - Bootstrapped")
    }

    func tap(action: String, system: Bool) {
        debugPrint("Engine - Tap, Action: \(action)")
    }

    func routeChanged(newRoute: String) {
        debugPrint("Engine - Route Changed \(newRoute)")
    }

    func routeError(route: String) {
        debugPrint("Engine - Route Error \(route)")
    }

    func routeLoaded(route: String) {
        debugPrint("Engine - Route Loaded \(route)")
    }

    func error() {
        debugPrint("Engine - Error")
    }
}
