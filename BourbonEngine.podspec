Pod::Spec.new do |s|
  s.name = 'BourbonEngine'
  s.version = '0.13.1'
  s.summary = 'Bourbon Engine'
  s.description = 'Bourbon Engine is a server powered rendering engine for mobile applications, it enables product teams to build, test and iterate on fully native applications instantly.'
  s.homepage = 'https://bourbon.sh'
  s.license = { :type => 'Bourbon Platform License', :file => 'LICENSE' }
  s.author = 'Bourbon Ltd' 
  s.swift_version = '5.0'

  s.ios.deployment_target = '10.0'

  s.source = { :git => 'https://gitlab.com/bourbonltd/engine-apple', :tag => s.version }

  s.source_files  = 'Bourbon Engine/*.{swift,h,m}', 'Bourbon Engine/**/*.{swift,h,m}'
  s.vendored_frameworks = 'Flutter/App.xcframework', 'Flutter/Flutter.xcframework', 'Flutter/FlutterPluginRegistrant.xcframework', 'Flutter/path_provider.xcframework', 'Flutter/shared_preferences.xcframework', 'Flutter/url_launcher.xcframework'
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'armv7 arm64 x86_64' }
  s.weak_frameworks = 'SwiftUI', 'Combine'
end
