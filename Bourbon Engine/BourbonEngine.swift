import Foundation
import Flutter
import FlutterPluginRegistrant

public protocol BourbonEngineDelegate: AnyObject {
    func bootstrapped()
    func tap(action: String, system: Bool)
    func routeChanged(newRoute: String)
    func routeError(route: String)
    func routeLoaded(route: String)
    func error()
}

public enum RouteBehaviour: String {
    case push
    case retain
    case system
}

public class BourbonEngine: BourbonEngineDelegate {
    private var _currentRoute = ""
    private var _flutterEngine: FlutterEngine!
    private var _engineFlutterBridge: EngineFlutterBridge

    weak public var delegate: BourbonEngineDelegate?

    public var viewController: UIViewController? {
        return EngineCoordinator(flutterEngine: _flutterEngine).viewController
    }

    #if (arch(x86_64) || arch(arm64))
    @available(iOS 13.0, *)
    public var engineView: EngineView {
        return EngineView(engine: _flutterEngine)
    }
    #endif

    public private(set) var currentRoute: String {
        get {
            return _currentRoute
        }
        set {
            _currentRoute = newValue
        }
    }

    public init(configuration: EngineConfiguration) {
        _flutterEngine = FlutterEngine(name: "io.flutter",
                                       project: FlutterDartProject(
                                        precompiledDartBundle: Bundle(for: BourbonEngine.self)))

        _flutterEngine?.run(withEntrypoint: nil)

        _engineFlutterBridge = EngineFlutterBridge(flutterEngine: _flutterEngine)
        _engineFlutterBridge.delegate = self

        GeneratedPluginRegistrant.register(with: _flutterEngine)

        _engineFlutterBridge.setup(engineConfiguration: configuration)
    }

    public func changeRoute(route: EngineRoute, behaviour: RouteBehaviour) {
        _currentRoute = route.route
        _engineFlutterBridge.updateRoute(route: route, behaviour: behaviour)
    }

    public func bootstrapped() {
        delegate?.bootstrapped()
    }

    public func error() {
        delegate?.error()
    }

    public func tap(action: String, system: Bool) {
        delegate?.tap(action: action, system: system)
    }

    public func routeLoaded(route: String) {
        delegate?.routeLoaded(route: route)
    }

    public func routeError(route: String) {
        delegate?.routeError(route: route)
    }

    public func routeChanged(newRoute: String) {
        _currentRoute = newRoute
        delegate?.routeChanged(newRoute: newRoute)
    }

    deinit {
        _flutterEngine.viewController = nil
        _flutterEngine.destroyContext()
    }
}
