import BourbonEngine
import SwiftUI

@main
struct DemoApp: App {
    public var bourbonEngine: BourbonEngine!
    // For demo purposes we're disabling this warning
    //swiftlint:disable weak_delegate
    var engineDelegate: BourbonEngineDelegate?
    //swiftlint:enable weak_delegate

    init() {
        let route = EngineRoute(route: "artists")
        route.addProperty(key: "title", value: "Top Artists")
        route.addProperty(key: "list", value: ArtistsMock.data)

        let engineConfiguration = EngineConfiguration(organizationId: "c6ff92b9-5607-4655-9265-f2588f7e3b58",
                                                      projectId: "61523087-2513-455f-9c15-2c5aee4f4482",
                                                      engineEndpoint: "https://engine.api.gist.build",
                                                      authenticationEndpoint: "https://identity.api.gist.build",
                                                      mainRoute: route,
                                                      engineVersion: 1,
                                                      configurationVersion: 1)

        bourbonEngine = BourbonEngine(configuration: engineConfiguration)
        engineDelegate = EngineEventHandler()
        bourbonEngine.delegate = engineDelegate
    }

    var body: some Scene {
        WindowGroup {
            ContentView(engine: bourbonEngine)
        }
    }
}
