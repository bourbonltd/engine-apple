import Foundation

struct EngineRouteChange: Encodable {
    public let route: String
    public let behaviour: String
    public let properties: [String: AnyEncodable?]?

    init(route: String, behaviour: RouteBehaviour, properties: [String: AnyEncodable?]? = nil) {
        self.route = route
        self.properties = properties
        self.behaviour = behaviour.rawValue
    }
}
