import Foundation
import UIKit
import Flutter

class EngineCoordinator {
    var flutterViewController: FlutterViewController!

    var viewController: UIViewController {
        return self.flutterViewController
    }

    init(flutterEngine: FlutterEngine) {
        self.flutterViewController = FlutterViewController(engine: flutterEngine, nibName: nil, bundle: nil)
        self.flutterViewController.isViewOpaque = false
    }
}
