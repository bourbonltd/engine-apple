import SwiftUI
import BourbonEngine

struct ContentView: View {
    var engine: BourbonEngine

    var body: some View {
        engine.engineView
        Button {
            let engineRoute = EngineRoute(route: "welcome")
            engineRoute.addProperty(key: "name", value: "John")
            engine.changeRoute(route: engineRoute,
                               behaviour: .retain)
        } label: {
            Text("Change Route")
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(engine: BourbonEngine(
                        configuration: EngineConfiguration(
                            organizationId: "",
                            projectId: "",
                            engineEndpoint: "",
                            authenticationEndpoint: "",
                            mainRoute: EngineRoute(route: ""),
                            engineVersion: 1.0,
                            configurationVersion: 1.0)))
    }
}
