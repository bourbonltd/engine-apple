import Foundation

public struct EngineConfiguration: Encodable {
    public let organizationId: String
    public let projectId: String
    public let engineEndpoint: String
    public let authenticationEndpoint: String
    public let engineVersion: Decimal
    public let configurationVersion: Decimal
    public let mainRoute: String?
    public let mainRouteProperties: [String: AnyEncodable?]?

    public init(organizationId: String,
                projectId: String,
                engineEndpoint: String,
                authenticationEndpoint: String,
                mainRoute: EngineRoute,
                engineVersion: Decimal,
                configurationVersion: Decimal) {
        self.organizationId = organizationId
        self.projectId = projectId
        self.engineEndpoint = engineEndpoint
        self.authenticationEndpoint = authenticationEndpoint
        self.engineVersion = engineVersion
        self.configurationVersion = configurationVersion
        self.mainRoute = mainRoute.route
        self.mainRouteProperties = mainRoute.properties
    }
}
