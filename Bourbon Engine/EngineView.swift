#if (arch(x86_64) || arch(arm64))
import SwiftUI
import Flutter

@available(iOS 13.0, *)
public struct EngineView: UIViewControllerRepresentable {
    let engine: FlutterEngine

    public func makeUIViewController(context: Context) -> FlutterViewController {
        return FlutterViewController(engine: engine, nibName: nil, bundle: nil)
    }

    public func updateUIViewController(_ uiViewController: FlutterViewController, context: Context) {}
}
#endif
